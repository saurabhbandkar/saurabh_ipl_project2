const matchesPerYear = require('./matchesPerYear.js');
const matchesWonPerTeamPerYear = require('./matchesWonPerTeamPerYear.js');
const runsConcededPerYear = require('./runsConcededPerYear.js');
const top10EconomicalBowlers = require('./top10EconomicalBowlers.js');
const tossWinMatchWin = require('./tossWinMatchWin.js');
const playerOfTheMatch = require('./playerOfTheMatch.js');

matchesPerYear.outputMatchesPerYear();
matchesWonPerTeamPerYear.outputMatchesWonPerTeamPerYear();
runsConcededPerYear.outputRunsConcededPerYear(2016);
top10EconomicalBowlers.outputTop10EconomicalBowlers(2015);