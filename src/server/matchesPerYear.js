function outputMatchesPerYear() {

    const csv = require('csv-parser');
    const fs = require('fs');
    let matchesData = [];
    let inputFilePath = '../data/matches.csv';

    fs.createReadStream(inputFilePath)
        .on('error', () => {
            console.error('Input file does not exist!!!');
        })
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))
        .on('end', () => {

            function matchesPerYear(dataSet) {

                let matchesCount = new Object();
                let years = new Array();
                let allYears = new Array();
                const dataSetLength = Array.from({ length: dataSet.length }, (value, index) => {
                    return index;
                });

                dataSetLength.map((currentIndex) => {
                    let currentYear = dataSet[currentIndex]['season'];

                    allYears.push(currentYear);
                    matchesCount[currentYear] = 0;
                });

                allYears.map((currentYear) => {
                    matchesCount[currentYear] +=1;
                });

                function saveFile(output) {

                    const outputData = JSON.stringify(output);
                    let outputFile = "../public/output/matchesPerYear.json";

                    fs.writeFile(outputFile, outputData, 'utf8', function (err) {
                        if (err) {
                            console.error(err);
                        }
                        else {
                            console.log('Successfully created file matchesPerYear.json');
                        };
                    });
                };
                saveFile(matchesCount);
            };
            matchesPerYear(matchesData);
        });
};
module.exports = { outputMatchesPerYear };