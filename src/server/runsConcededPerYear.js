function outputRunsConcededPerYear(requiredYear) {
    const csv = require('csv-parser');
    const fs = require('fs');
    const deliveriesData = new Array();
    const matchesData = new Array();
    let matchesInputFilePath = '../data/matches.csv';
    let deliveriesInputFilePath = '../data/deliveries.csv';

    fs.createReadStream(matchesInputFilePath)
        .on('error', () => {
            console.error('Input file matches does not exist!!!');
        })
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))
        .on('end', () => {

            function getYear(dataSetMatches) {

                let yearId = new Object();
                let years = new Array();
                const dataSetMatchesLength = Array.from({ length: dataSetMatches.length }, (value, index) => {
                    return index;
                });

                dataSetMatchesLength.map((currentMatchesIndex) => {
                    if (!years.includes(dataSetMatches[currentMatchesIndex]['season'])) {
                        years.push(dataSetMatches[currentMatchesIndex]['season']);
                    };
                });

                years.map((year) => {
                    let id = new Array();

                    dataSetMatchesLength.map((matchesIndex) => {
                        let currentYear = year;
                        let currentDataBaseYear = dataSetMatches[matchesIndex]['season'];

                        if (currentYear == currentDataBaseYear) {
                            id.push(dataSetMatches[matchesIndex]['id']);
                        };
                    });
                    yearId[year] = id;
                });

                let teams = new Array();

                dataSetMatchesLength.map((currentMatchesIndex) => {

                    let team1 = dataSetMatches[currentMatchesIndex]['team1'];
                    let team2 = dataSetMatches[currentMatchesIndex]['team2'];

                    if (!teams.includes(team1)) {
                        teams.push(team1);
                    };
                    if (!teams.includes(team2)) {
                        teams.push(team2);
                    };
                });

                fs.createReadStream(deliveriesInputFilePath)
                    .on('error', () => {
                        console.error('Input file matches does not exist!!!');
                    })
                    .pipe(csv())
                    .on('data', (data) => deliveriesData.push(data))
                    .on('end', () => {

                        let teamExtraRuns = new Object();

                        function extraRunsPerTeam(dataSet) {

                            const dataSetDeliveriesLength = Array.from({ length: dataSet.length }, (value, index) => {
                                return index;
                            });

                            teams.map((team) => {
                                let extraRuns = 0;

                                dataSetDeliveriesLength.map((currentDeliveriesIndex) => {
                                    let currentTeam = dataSet[currentDeliveriesIndex]['bowling_team'];
                                    let currentId = dataSet[currentDeliveriesIndex]['match_id'];
                                    let currentExtras = dataSet[currentDeliveriesIndex]['extra_runs'];

                                    if ((yearId[requiredYear].includes(currentId)) && (currentTeam == team)) {
                                        extraRuns = extraRuns + parseInt(currentExtras);
                                    };
                                });
                                teamExtraRuns[team] = extraRuns;
                            });
                        };

                        extraRunsPerTeam(deliveriesData);
                        function saveFile(output) {

                            const outputData = JSON.stringify(output);
                            let outputFile = "../public/output/extraRunsPerTeam.json";

                            fs.writeFile(outputFile, outputData, 'utf8', function (err) {
                                if (err) {
                                    console.error(err);
                                }
                                else {
                                    console.log('Successfully created file extraRunsPerTeam.json');
                                };
                            });
                        };
                        saveFile(teamExtraRuns);
                    });
            };
            getYear(matchesData);
        });
};
module.exports = { outputRunsConcededPerYear };