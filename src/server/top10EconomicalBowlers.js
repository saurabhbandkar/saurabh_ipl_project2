function outputTop10EconomicalBowlers(requiredYear) {

    const csv = require('csv-parser');
    const fs = require('fs');
    let deliveriesData = new Array();
    let matchesData = new Array();
    let matchesInputFilePath = '../data/matches.csv';
    let deliveriesInputFilePath = '../data/deliveries.csv';

    fs.createReadStream(matchesInputFilePath)
        .on('error', () => {
            console.error('Input file matches does not exist!!!');
        })
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))
        .on('end', () => {

            function getYear(dataSetMatches) {

                let yearId = new Object();
                let years = new Array();
                const dataSetMatchesLength = Array.from({ length: dataSetMatches.length }, (value, index) => {
                    return index;
                });

                dataSetMatchesLength.map((matchesIndex) => {
                    if (!years.includes(dataSetMatches[matchesIndex]['season'])) {
                        years.push(dataSetMatches[matchesIndex]['season']);
                    };
                });

                years.map((year) => {
                    let id = new Array();

                    dataSetMatchesLength.map((currentMatchesIndex) => {

                        let currentYear = year;
                        let currentDataBaseYear = dataSetMatches[currentMatchesIndex]['season'];

                        if (currentYear == currentDataBaseYear) {
                            id.push(dataSetMatches[currentMatchesIndex]['id']);
                        };
                    });
                    yearId[year] = id;
                });

                fs.createReadStream(deliveriesInputFilePath)
                    .on('error', () => {
                        console.error('Input file deliveries does not exist!!!');
                    })
                    .pipe(csv())
                    .on('data', (data) => deliveriesData.push(data))
                    .on('end', () => {

                        function economyRate(dataSet) {

                            let bowlersList = new Array();

                            const dataSetDeliveriesLength = Array.from({ length: dataSet.length }, (value, index) => {
                                return index;
                            });

                            dataSetDeliveriesLength.map((currentDeliveriesIndex) => {
                                let currentBowler = dataSet[currentDeliveriesIndex]['bowler'];

                                if (!bowlersList.includes(currentBowler)) {
                                    bowlersList.push(currentBowler);
                                };
                            });

                            const bowlerEconomy = new Array();

                            bowlersList.map((bowler) => {
                                let currentBowler = bowler;
                                let economy = 0;
                                let overs = 0;
                                let runs = 0;

                                dataSetDeliveriesLength.map((deliveriesIndex) => {
                                    let currentDataBaseBowler = dataSet[deliveriesIndex]['bowler'];
                                    let currentId = dataSet[deliveriesIndex]['match_id'];
                                    let currentWides = dataSet[deliveriesIndex]['wide_runs'];
                                    let currentNoBalls = dataSet[deliveriesIndex]['noball_runs'];
                                    let currentBatsmen = dataSet[deliveriesIndex]['batsman_runs'];

                                    if (yearId[requiredYear].includes(currentId)) {
                                        if (currentBowler == currentDataBaseBowler) {
                                            runs = runs + parseInt(currentWides) + parseInt(currentNoBalls) + parseInt(currentBatsmen);
                                        };
                                        if (dataSet.length - deliveriesIndex !== 1) {
                                            if ((currentBowler == currentDataBaseBowler) && (currentDataBaseBowler !== dataSet[deliveriesIndex + 1]['bowler'])) {
                                                overs += 1;
                                            };
                                        };
                                    };
                                });
                                if (overs > 0) {
                                    economy = runs / overs;
                                    bowlerEconomy.push([currentBowler, parseFloat(economy.toFixed(2))]);
                                };
                            });

                            bowlerEconomy.sort(function (x, y) {
                                return x[1] - y[1];
                            });

                            let top10BowlerEconomy = bowlerEconomy.slice(0, 10);

                            function saveFile(output) {

                                const outputData = JSON.stringify(output);
                                let outputFile = "../public/output/bowlerEconomy.json";

                                fs.writeFile(outputFile, outputData, 'utf8', function (err) {
                                    if (err) {
                                        console.error(err);
                                    }
                                    else {
                                        console.log('Successfully created file bowlerEconomy.json');
                                    };
                                });
                            };
                            saveFile(top10BowlerEconomy);
                        };
                        economyRate(deliveriesData);
                    });
            };
            getYear(matchesData);
        });
};
module.exports = { outputTop10EconomicalBowlers };
