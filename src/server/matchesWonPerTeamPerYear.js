function outputMatchesWonPerTeamPerYear() {

    const csv = require('csv-parser');
    const fs = require('fs');
    let matchesData = new Array();
    let inputFilePath = '../data/matches.csv';
    fs.createReadStream(inputFilePath)
        .on('error', () => {
            console.error('Input file does not exist!!!');
        })
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))

        .on('end', () => {

            function matchesPerTeamPerYear(dataSet) {

                let matchesCount = new Object();
                let years = new Array();
                let teamsList = new Array();
                let allYears = new Array();
                const dataSetLength = Array.from({ length: dataSet.length }, (value, index) => {
                    return index;
                });

                dataSetLength.map((currentIndex) => {

                    let currentYear = dataSet[currentIndex]['season'];
                    let team1 = dataSet[currentIndex]['team1'];
                    let team2 = dataSet[currentIndex]['team2'];

                    if (!teamsList.includes(team1)) {
                        teamsList.push(team1);
                    };
                    if (!teamsList.includes(team2)) {
                        teamsList.push(team2);
                    };
                    if (!years.includes(currentYear)) {
                        years.push(currentYear);
                    };
                });

                teamsList.map((currentTeam) => {
                    let result = new Array();

                    years.map((year) => {
                        let counter = 0;

                        dataSetLength.map((dataSetIndex) => {
                            let currentYear = dataSet[dataSetIndex]['season'];
                            let currentWinner = dataSet[dataSetIndex]['winner'];

                            if ((currentYear == year) && (currentWinner == currentTeam)) {
                                counter += 1;
                            };
                        });
                        result.push([year, counter]);
                    });
                    matchesCount[currentTeam] = result;
                });
                function saveFile(output) {

                    const outputData = JSON.stringify(output);
                    let outputFile = "../public/output/matchesPerTeamPerYear.json";

                    fs.writeFile(outputFile, outputData, 'utf8', function (err) {
                        if (err) {
                            console.error(err);
                        }
                        else {
                            console.log('Successfully created file matchesPerTeamPerYear.json');
                        };
                    });
                };
                saveFile(matchesCount);
            };
            matchesPerTeamPerYear(matchesData);
        });
};
module.exports = { outputMatchesWonPerTeamPerYear };